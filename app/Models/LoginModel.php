<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
    protected $table = 'user';
    
    protected $allowedFields = ['username', 'password'];

    public function register() {
        $data['username'] = $this->input->post('name');
        $data['password'] = $this->input->post('password');
        $data['firstname'] = $this->input->post('firstname');
        $data['lastname'] = $this->input->post('lastname');
        $this->db->insert($table, $data);
    }

    public function check($username, $password) {
        $this->where('username',$username);
        $query = $this->get();
        // print $this->getLastQuery(); //useful for debugging
        $row = $query->getRow();
        if ($row) { //if row was returned based on username 
            //salasanan tarkistus
            if (password_verify($password,$row->password)) {
                return $row; // return row (user object)
            }
        }
        return null; // return null, bc username and/or psswrd is incorrect
    }
}